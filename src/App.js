import React, { Component } from 'react';
import './App.css';
import Game from './components/Game';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <h1>Welcome to tic tac toe</h1>
        <Game/>
      </React.Fragment>
    );
  }
}

export default App;
